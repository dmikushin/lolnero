Lolnero is a code fork of the cryptocurreny Wownero with a linear emission and a SHA-3 PoW.

There is no premine and no dev tax.


Why
===

The goal of Lolnero is to replace `C/C++` with a safer language, and to not hardfork.


Specifications
==============

* Proof of Work: SHA-3
* Max supply: ∞
* Block reward: 300
* Block time: 5 minutes
* Block size limit: max(128k, Block height) bytes
* Confidential transaction: Bulletproofs
* Ring signature: CLSAG
* Ring size: 32

Building on Ubuntu
==================

Prerequisites:

```
sudo apt install cmake libboost1.74-dev rapidjson-dev opencl-clhpp-headers
```

C++20 compiler is required, such as g++-11

Building:

```
git clone --recurse-submodules https://gitlab.com/dmikushin/lolnero.git
cd lolnero
mkdir build
cd build
cmake .. -DUSE_OPENCL=ON -G Ninja -DCMAKE_CXX_COMPILER=g++-11
ninja
```

# [How to build](https://lolnero.org/build.html)

# [Seed nodes](https://gitlab.com/lolnero/lolnero/-/wikis/Seed-nodes)

Deployment
==========

First, create a new wallet by running:

```
bin/lolnero
```

Next, connect to the network and start mining:

```
bin/lolnerod --seed-node 128.199.161.251
start-mining WALLET_ADDRESS
```

