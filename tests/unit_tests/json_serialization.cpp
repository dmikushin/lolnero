
#include <optional>
#include <boost/range/adaptor/indexed.hpp>
#include <gtest/gtest.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <vector>

#include "math/crypto/functional/hash.hpp"
#include "cryptonote/basic/account.h"
#include "cryptonote/basic/cryptonote_basic.h"
#include "cryptonote/basic/cryptonote_format_utils.h"
#include "cryptonote/tx/pseudo_functional/tx_utils.hpp"
#include "tools/serialization/json_object.h"


namespace test
{
    cryptonote::transaction
    make_miner_transaction(cryptonote::account_public_address const& to)
    {
        const auto tx = cryptonote::construct_miner_tx(0, 0, 500, to);
        if (!tx) {
            throw std::runtime_error{"transaction construction error"};
        }

        // cryptonote::get_transaction_hash(*tx);

        return *tx;
    }

    cryptonote::transaction
    make_transaction(
        cryptonote::account_keys const& from,
        std::vector<cryptonote::transaction> const& sources,
        std::vector<cryptonote::account_public_address> const& destinations
    )
    {
        std::uint64_t source_amount = 0;
        std::vector<cryptonote::tx_source_entry> actual_sources;
        for (auto const& source : sources)
        {
            const auto extra_fields = cryptonote::parse_tx_extra(source.extra);
            if (!extra_fields)
                throw std::runtime_error{"invalid transaction"};

            cryptonote::tx_extra_tx_public_key key_field{};
            if (!cryptonote::find_tx_extra_field_by_type(*extra_fields, key_field))
                throw std::runtime_error{"invalid transaction"};

            for (auto const& input : boost::adaptors::index(source.vout))
            {
                source_amount += input.value().amount;
                auto const& key = boost::get<cryptonote::txout_to_key>(input.value().target);

                actual_sources.push_back(
                    {
                      {}, 0, key_field.pub_key_unsafe, {}
                     , std::size_t(input.index()), input.value().amount, true, rct::s_one
                    }
                );

                for (unsigned ring = 0; ring < 31; ++ring)
                  actual_sources.back().push_output(input.index(), key.output_spend_public_key, input.value().amount);
            }
        }

        std::vector<cryptonote::tx_destination_entry> to;
        for (auto const& destination : destinations)
            to.push_back({(source_amount / destinations.size()), destination, false});

        cryptonote::transaction tx{};

        std::vector<crypto::secret_key> extra_keys{};

        std::unordered_map<crypto::public_key, cryptonote::subaddress_index> subaddresses;
        subaddresses[from.m_account_address.m_spend_public_key] = {0,0};

        const auto r = cryptonote::construct_tx_and_get_tx_key
          (from, subaddresses, actual_sources, to, {}, 0);

        if (!r) {
          throw std::runtime_error{"transaction construction error"};
        }

        return std::get<0>(*r);
    }
}

namespace
{
    template<typename T>
    T test_json(const T& value)
    {
      rapidjson::StringBuffer buffer;
      {
        rapidjson::Writer<rapidjson::StringBuffer> dest{buffer};
        cryptonote::json::toJsonValue(dest, value);
      }

      rapidjson::Document doc;
      doc.Parse(buffer.GetString());
      if (doc.HasParseError() || !doc.IsObject())
      {
        throw cryptonote::json::PARSE_FAIL();
      }

      T out{};
      cryptonote::json::fromJsonValue(doc, out);
      return out;
    }
} // anonymous

TEST(JsonSerialization, MinerTransaction)
{
    cryptonote::account_base acct;
    acct.generate();
    const auto miner_tx = test::make_miner_transaction(acct.get_keys().m_account_address);

    crypto::hash tx_hash = cryptonote::get_transaction_hash(miner_tx);

    cryptonote::transaction miner_tx_copy = test_json(miner_tx);

    crypto::hash tx_copy_hash = cryptonote::get_transaction_hash(miner_tx_copy);
    EXPECT_EQ(tx_hash, tx_copy_hash);

    const auto tx_bytes = cryptonote::t_serializable_object_to_maybe_blob(miner_tx);
    const auto tx_copy_bytes = cryptonote::t_serializable_object_to_maybe_blob(miner_tx_copy);

    ASSERT_TRUE(tx_bytes);
    ASSERT_TRUE(tx_copy_bytes);

    EXPECT_EQ(*tx_bytes, *tx_copy_bytes);
}

TEST(JsonSerialization, BulletproofTransaction)
{
    cryptonote::account_base acct1;
    acct1.generate();

    cryptonote::account_base acct2;
    acct2.generate();

    const auto miner_tx = test::make_miner_transaction(acct1.get_keys().m_account_address);
    const auto tx = test::make_transaction(
        acct1.get_keys(), {miner_tx}, {acct2.get_keys().m_account_address}
    );

    cryptonote::get_transaction_hash(tx);

    cryptonote::transaction tx_copy = test_json(tx);

    // serialization will throw for some reason
    // cryptonote::get_transaction_hash(tx_copy);

    // TODO fix test failure
    // EXPECT_EQ(tx_hash, tx_copy_hash);

    // cryptonote::blobdata tx_bytes{};
    // cryptonote::blobdata tx_copy_bytes{};

    const auto tx_bytes = cryptonote::t_serializable_object_to_maybe_blob(tx);
    ASSERT_TRUE(tx_bytes);


    // TODO fix test failure
    // ASSERT_TRUE(cryptonote::t_serializable_object_to_blob(tx_copy, tx_copy_bytes));

    // TODO fix test failure
    // EXPECT_EQ(tx_bytes, tx_copy_bytes);
}

