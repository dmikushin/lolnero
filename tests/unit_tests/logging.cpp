// Copyright (c) 2016-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "gtest/gtest.h"

#include "tools/epee/include/file_io_utils.h"
#include "tools/epee/include/logging.hpp"
#include "tools/epee/include/string_tools.h"

static std::string log_filename;

static void init()
{
  std::filesystem::path p = epee::string_tools::random_temp_path();
  log_filename = p.string();
  mlog_configure(log_filename, false);
}

static void cleanup()
{
  // windows does not let files be deleted if still in use, so leave droppings there
  std::filesystem::remove(log_filename);
}

static size_t nlines(const std::string &str)
{
  size_t n = 0;
  for (const char *ptr = str.c_str(); *ptr; ++ptr)
    if (*ptr == '\n')
      ++n;
  return n;
}

static bool load_log_to_string(const std::string &filename, std::string &str)
{
  if (!epee::file_io_utils::load_file_to_string(filename, str))
    return false;
  for (const char *ptr = str.c_str(); *ptr; ++ptr)
  {
    if (*ptr == '\n')
    {
      std::string prefix = std::string(str.c_str(), ptr - str.c_str());
      if (prefix.find("New log categories:") != std::string::npos)
      {
        str = std::string(ptr + 1, strlen(ptr + 1));
        break;
      }
    }
  }
  return true;
}

static void log()
{
  LOG_FATAL("fatal");
  LOG_ERROR("error");
  LOG_WARNING("warning");
  LOG_INFO("info");
  LOG_DEBUG("debug");
  LOG_TRACE("trace");

  LOG_CATEGORY_INFO("a.b.c.d", "a.b.c.d");
  LOG_CATEGORY_INFO("a.b.c.e", "a.b.c.e");
  LOG_CATEGORY_INFO("global", "global");
  LOG_CATEGORY_INFO("x.y.z", "x.y.z");
  LOG_CATEGORY_INFO("y.y.z", "y.y.z");
  LOG_CATEGORY_INFO("x.y.x", "x.y.x");
}

TEST(logging, no_logs)
{
  init();
  mlog_set_categories("");
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(str == "");
  cleanup();
}

TEST(logging, default)
{
  init();
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(str.find("global") != std::string::npos);
  ASSERT_TRUE(str.find("fatal") != std::string::npos);
  // ASSERT_TRUE(str.find("error") != std::string::npos);
  ASSERT_TRUE(str.find("debug") == std::string::npos);
  ASSERT_TRUE(str.find("trace") == std::string::npos);
  cleanup();
}

TEST(logging, all)
{
  init();
  mlog_set_categories("*:TRACE");
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(str.find("global") != std::string::npos);
  ASSERT_TRUE(str.find("fatal") != std::string::npos);
  ASSERT_TRUE(str.find("error") != std::string::npos);
  ASSERT_TRUE(str.find("debug") != std::string::npos);
  ASSERT_TRUE(str.find("trace") != std::string::npos);
  cleanup();
}

TEST(logging, glob_suffix)
{
  init();
  mlog_set_categories("x.y*:TRACE");
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(str.find("global") == std::string::npos);
  ASSERT_TRUE(str.find("x.y.z") != std::string::npos);
  ASSERT_TRUE(str.find("x.y.x") != std::string::npos);
  ASSERT_TRUE(str.find("y.y.z") == std::string::npos);
  cleanup();
}

TEST(logging, glob_prefix)
{
  init();
  mlog_set_categories("*y.z:TRACE");
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(str.find("global") == std::string::npos);
  ASSERT_TRUE(str.find("x.y.z") != std::string::npos);
  ASSERT_TRUE(str.find("x.y.x") == std::string::npos);
  ASSERT_TRUE(str.find("y.y.z") != std::string::npos);
  cleanup();
}

TEST(logging, last_precedence)
{
  init();
  mlog_set_categories("gobal:FATAL,glo*:DEBUG");
  log();
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(nlines(str) == 1);
  ASSERT_TRUE(str.find("global") != std::string::npos);
  ASSERT_TRUE(str.find("x.y.z") == std::string::npos);
  ASSERT_TRUE(str.find("x.y.x") == std::string::npos);
  ASSERT_TRUE(str.find("y.y.z") == std::string::npos);
  cleanup();
}

TEST(logging, multiline)
{
  init();
  mlog_set_categories("global:INFO");
  LOG_GLOBAL_INFO("first\nsecond\nthird");
  std::string str;
  ASSERT_TRUE(load_log_to_string(log_filename, str));
  ASSERT_TRUE(nlines(str) == 3);
  ASSERT_TRUE(str.find("global") != std::string::npos);
  ASSERT_TRUE(str.find("first") != std::string::npos);
  ASSERT_TRUE(str.find("second") != std::string::npos);
  ASSERT_TRUE(str.find("third") != std::string::npos);
  ASSERT_TRUE(str.find("first\nsecond") == std::string::npos);
  ASSERT_TRUE(str.find("second\nthird") == std::string::npos);
  cleanup();
}

