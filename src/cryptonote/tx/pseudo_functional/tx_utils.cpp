// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "tx_utils.hpp"

#include "tools/epee/include/string_tools.h"

#include "math/crypto/controller/random.hpp"

#include "math/ringct/pseudo_functional/rctSigs.hpp"
#include "math/ringct/controller/rctSigGen.hpp"

#include "cryptonote/basic/functional/subaddress.hpp"



namespace cryptonote
{
  //---------------------------------------------------------------
  std::optional<
    std::tuple<
      transaction
      , std::vector<tx_source_entry>
      , std::vector<crypto::secret_key>
      >>

  construct_tx_and_get_tx_key
  (
   const account_keys sender_account_keys
   , const std::unordered_map<crypto::public_key, subaddress_index>& subaddresses
   , const std::vector<tx_source_entry> sources
   , const std::span<tx_destination_entry> destinations
   , const std::vector<uint8_t> extra
   , const uint64_t unlock_time
   )
  {
    std::vector<crypto::secret_key> output_secret_keys;

    {
      output_secret_keys.clear();
      output_secret_keys.resize(destinations.size());
      std::generate(output_secret_keys.begin(), output_secret_keys.end(),
                    []() {
                      return keypair::generate().sec;
                    });
    }

    const auto& r = construct_tx_with_tx_key
      (
       sender_account_keys
       , subaddresses
       , sources
       , destinations
       , extra
       , unlock_time
       , output_secret_keys
       );
    if (r) {
      const auto [tx, sources] = *r;
      return {{tx, sources, output_secret_keys}};
    }

    return {};
  }
}
