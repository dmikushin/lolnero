#include "config/version.hpp"

constexpr std::string_view LOLNERO_VERSION = "0.9.9.9";
constexpr std::string_view LOLNERO_RELEASE_NAME = "Saber";
