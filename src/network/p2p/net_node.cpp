// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "net_node.h"

#include "network/type/socks.h"
#include "network/type/parse.h"

#include "cryptonote/core/cryptonote_core.h"



#include <boost/endian/conversion.hpp>

#include <boost/algorithm/string/find_iterator.hpp>



namespace
{
    constexpr const std::chrono::milliseconds future_poll_interval{500};
    constexpr const std::chrono::seconds socks_connect_timeout{P2P_DEFAULT_SOCKS_CONNECT_TIMEOUT};

    std::int64_t get_max_connections(const boost::iterator_range<std::string_view::const_iterator> value) noexcept
    {
        // -1 is default, 0 is error
        if (value.empty())
            return -1;

        std::uint32_t out = 0;
        if (epee::string_tools::get_xtype_from_string(out, std::string{value.begin(), value.end()}))
            return out;
        return 0;
    }

    bool start_socks(std::shared_ptr<net::socks::client> client, const boost::asio::ip::tcp::endpoint& proxy, const epee::net_utils::network_address& remote)
    {
        LOG_ERROR_AND_RETURN_UNLESS(client != nullptr, false, "Unexpected null client");

        bool set = false;
        switch (remote.get_type_id())
        {
        case net::tor_address::get_type_id():
            set = client->set_connect_command(remote.as<net::tor_address>());
            break;
        case net::i2p_address::get_type_id():
            set = client->set_connect_command(remote.as<net::i2p_address>());
            break;
        case epee::net_utils::ipv4_network_address::get_type_id():
            set = client->set_connect_command(remote.as<epee::net_utils::ipv4_network_address>());
            break;
        default:
            LOG_ERROR("Unsupported network address in socks_connect");
            return false;
        }

        const bool sent =
            set && net::socks::client::connect_and_send(std::move(client), proxy);
        LOG_ERROR_AND_RETURN_UNLESS(sent, false, "Unexpected failure to init socks client");
        return true;
    }
}

namespace nodetool
{
  boost::asio::ip::address_v4 make_address_v4_from_v6(const boost::asio::ip::address_v6& a)
  {
    const auto &bytes = a.to_bytes();
    uint32_t v4 = 0;
    v4 = (v4 << 8) | bytes[12];
    v4 = (v4 << 8) | bytes[13];
    v4 = (v4 << 8) | bytes[14];
    v4 = (v4 << 8) | bytes[15];
    return boost::asio::ip::address_v4(v4);
  }


    const command_line::arg_descriptor<std::string> arg_p2p_bind_ip        = {"p2p-bind-ip", "Interface for p2p network protocol (IPv4)", "0.0.0.0"};
    const command_line::arg_descriptor<std::string> arg_p2p_bind_ipv6_address        = {"p2p-bind-ipv6-address", "Interface for p2p network protocol (IPv6)", "::"};
    const command_line::arg_descriptor<std::string, false, true> arg_p2p_bind_port = {
        "p2p-bind-port"
      , "Port for p2p network protocol (IPv4)"
      , std::to_string(cryptonote::mainnet.P2P_DEFAULT_PORT)
      , cryptonote::arg_testnet_on
      , [](bool testnet, bool defaulted, std::string val)->std::string {
        if (testnet && defaulted)
          return std::to_string(cryptonote::testnet.P2P_DEFAULT_PORT);
        return val;
      }
    };
    const command_line::arg_descriptor<std::string, false, true> arg_p2p_bind_port_ipv6 = {
        "p2p-bind-port-ipv6"
      , "Port for p2p network protocol (IPv6)"
      , std::to_string(cryptonote::mainnet.P2P_DEFAULT_PORT)
      , cryptonote::arg_testnet_on
      , [](bool testnet, bool defaulted, std::string val)->std::string {
        if (testnet && defaulted)
          return std::to_string(cryptonote::testnet.P2P_DEFAULT_PORT);
        return val;
      }
    };

    const command_line::arg_descriptor<uint32_t>    arg_p2p_external_port  = {"p2p-external-port", "External port for p2p network protocol (if port forwarding used with NAT)", 0};
    const command_line::arg_descriptor<bool>        arg_p2p_allow_local_ip = {"allow-local-ip", "Allow local ip add to peer list, mostly in debug purposes"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_p2p_add_peer   = {"add-peer", "Manually add peer to local peerlist"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_p2p_add_priority_node   = {"add-priority-node", "Specify list of peers to connect to and attempt to keep the connection open"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_p2p_add_exclusive_node   = {"add-exclusive-node", "Specify list of peers to connect to only."
                                                                                                  " If this option is given the options add-priority-node and seed-node are ignored"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_p2p_seed_node   = {"seed-node", "Connect to a node to retrieve peer addresses, and disconnect"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_proxy = {"proxy", "Send local txes through proxy: <network-type>,<socks-ip:port>[,max_connections]"};
    const command_line::arg_descriptor<std::vector<std::string> > arg_anonymous_inbound = {"anonymous-inbound", "<hidden-service-address>,<[bind-ip:]port>[,max_connections] i.e. \"x.onion,127.0.0.1:18083,100\""};
    const command_line::arg_descriptor<bool> arg_p2p_hide_my_port   =    {"hide-my-port", "Do not announce yourself as peerlist candidate", false, true};
    const command_line::arg_descriptor<bool> arg_no_sync = {"no-sync", "Don't synchronize the blockchain with other peers", false};

    const command_line::arg_descriptor<bool>        arg_p2p_use_ipv6  = {"p2p-use-ipv6", "Enable IPv6 for p2p", false};
    const command_line::arg_descriptor<bool>        arg_p2p_ignore_ipv4  = {"p2p-ignore-ipv4", "Ignore unsuccessful IPv4 bind for p2p", false};
    const command_line::arg_descriptor<int64_t>     arg_out_peers = {"out-peers", "set max number of out peers", -1};
    const command_line::arg_descriptor<int64_t>     arg_in_peers = {"in-peers", "set max number of in peers", -1};
    const command_line::arg_descriptor<int> arg_tos_flag = {"tos-flag", "set TOS flag", -1};

    std::optional<std::vector<proxy>> get_proxies(boost::program_options::variables_map const& vm)
    {
        namespace ip = boost::asio::ip;

        std::vector<proxy> proxies{};

        const std::vector<std::string> args = command_line::get_arg(vm, arg_proxy);
        proxies.reserve(args.size());

        for (const std::string_view arg : args)
        {
            proxies.emplace_back();

            auto next = boost::algorithm::make_split_iterator(arg, boost::algorithm::first_finder(","));
            LOG_ERROR_AND_RETURN_UNLESS(!next.eof() && !next->empty(), std::nullopt, "No network type for --" << arg_proxy.name);
            const std::string_view zone{next->begin(), next->size()};

            ++next;
            LOG_ERROR_AND_RETURN_UNLESS(!next.eof() && !next->empty(), std::nullopt, "No ipv4:port given for --" << arg_proxy.name);
            const std::string_view proxy{next->begin(), next->size()};

            ++next;
            for (unsigned count = 0; !next.eof(); ++count, ++next)
            {
                if (2 <= count)
                {
                    LOG_ERROR("Too many ',' characters given to --" << arg_proxy.name);
                    return std::nullopt;
                }

                if (std::string_view{next->begin(), next->size()} == "disable_noise")
                    proxies.back().noise = false;
                else
                {
                    proxies.back().max_connections = get_max_connections(*next);
                    if (proxies.back().max_connections == 0)
                    {
                        LOG_ERROR("Invalid max connections given to --" << arg_proxy.name);
                        return std::nullopt;
                    }
                }
            }

            const epee::net_utils::zone _zone = epee::net_utils::zone_from_string(zone);
            if (_zone == epee::net_utils::zone::invalid) {
              LOG_ERROR("Invalid network for --" << arg_proxy.name);
              return std::nullopt;
            }

            proxies.back().zone = _zone;

            std::uint32_t ip = 0;
            std::uint16_t port = 0;
            if (!epee::string_tools::parse_peer_from_string(ip, port, std::string{proxy}) || port == 0)
            {
                LOG_ERROR("Invalid ipv4:port given for --" << arg_proxy.name);
                return std::nullopt;
            }
            proxies.back().address = ip::tcp::endpoint{ip::address_v4{boost::endian::native_to_big(ip)}, port};
        }

        return proxies;
    }

    std::optional<std::vector<anonymous_inbound>> get_anonymous_inbounds(boost::program_options::variables_map const& vm)
    {
        std::vector<anonymous_inbound> inbounds{};

        const std::vector<std::string> args = command_line::get_arg(vm, arg_anonymous_inbound);
        inbounds.reserve(args.size());

        for (const std::string_view arg : args)
        {
            inbounds.emplace_back();

            auto next = boost::algorithm::make_split_iterator(arg, boost::algorithm::first_finder(","));
            LOG_ERROR_AND_RETURN_UNLESS(!next.eof() && !next->empty(), std::nullopt, "No inbound address for --" << arg_anonymous_inbound.name);
            const std::string_view address{next->begin(), next->size()};

            ++next;
            LOG_ERROR_AND_RETURN_UNLESS(!next.eof() && !next->empty(), std::nullopt, "No local ipv4:port given for --" << arg_anonymous_inbound.name);
            const std::string_view bind{next->begin(), next->size()};

            const std::size_t colon = bind.find_first_of(':');
            LOG_ERROR_AND_RETURN_UNLESS(colon < bind.size(), std::nullopt, "No local port given for --" << arg_anonymous_inbound.name);

            ++next;
            if (!next.eof())
            {
                inbounds.back().max_connections = get_max_connections(*next);
                if (inbounds.back().max_connections == 0)
                {
                    LOG_ERROR("Invalid max connections given to --" << arg_proxy.name);
                    return std::nullopt;
                }
            }

            expect<epee::net_utils::network_address> our_address = net::get_network_address(address, 0);
            switch (our_address ? our_address->get_type_id() : epee::net_utils::address_type::invalid)
            {
            case net::tor_address::get_type_id():
                inbounds.back().our_address = std::move(*our_address);
                inbounds.back().default_remote = net::tor_address::unknown();
                break;
            case net::i2p_address::get_type_id():
                inbounds.back().our_address = std::move(*our_address);
                inbounds.back().default_remote = net::i2p_address::unknown();
                break;
            default:
                LOG_ERROR("Invalid inbound address (" << address << ") for --" << arg_anonymous_inbound.name << ": " << (our_address ? "invalid type" : our_address.error().message()));
                return std::nullopt;
            }

            // get_address returns default constructed address on error
            if (inbounds.back().our_address == epee::net_utils::network_address{})
                return std::nullopt;

            std::uint32_t ip = 0;
            std::uint16_t port = 0;
            if (!epee::string_tools::parse_peer_from_string(ip, port, std::string{bind}))
            {
                LOG_ERROR("Invalid ipv4:port given for --" << arg_anonymous_inbound.name);
                return std::nullopt;
            }
            inbounds.back().local_ip = std::string{bind.substr(0, colon)};
            inbounds.back().local_port = std::string{bind.substr(colon + 1)};
        }

        return inbounds;
    }

    bool is_filtered_command(const epee::net_utils::network_address& address, int command)
    {
        switch (command)
        {
        case nodetool::COMMAND_HANDSHAKE_T<cryptonote::CORE_SYNC_DATA>::ID:
        case nodetool::COMMAND_TIMED_SYNC_T<cryptonote::CORE_SYNC_DATA>::ID:
        case cryptonote::NOTIFY_NEW_TRANSACTIONS::ID:
            return false;
        default:
            break;
        }

        switch(address.get_zone()) {
          case epee::net_utils::zone::public_:
          case epee::net_utils::zone::tor:
          case epee::net_utils::zone::i2p:
            return false;
          default:
            LOG_WARNING("Filtered command (#" << command << ") to/from " << address.str());
            return true;
        }
    }

    std::optional<boost::asio::ip::tcp::socket>
    socks_connect_internal(const std::atomic<bool>& stop_signal, boost::asio::io_service& service, const boost::asio::ip::tcp::endpoint& proxy, const epee::net_utils::network_address& remote)
    {
        using socket_type = net::socks::client::stream_type::socket;
        using client_result = std::pair<boost::system::error_code, socket_type>;

        struct notify
        {
            std::promise<client_result> socks_promise;

            void operator()(boost::system::error_code error, socket_type&& sock)
            {
                socks_promise.set_value(std::make_pair(error, std::move(sock)));
            }
        };

        std::future<client_result> socks_result{};
        {
            std::promise<client_result> socks_promise{};
            socks_result = socks_promise.get_future();

            auto client = net::socks::make_connect_client(
                boost::asio::ip::tcp::socket{service}, net::socks::version::v4a, notify{std::move(socks_promise)}
             );
            if (!start_socks(std::move(client), proxy, remote))
                return std::nullopt;
        }

        const auto start = std::chrono::steady_clock::now();
        while (socks_result.wait_for(future_poll_interval) == std::future_status::timeout)
        {
            if (socks_connect_timeout < std::chrono::steady_clock::now() - start)
            {
                LOG_ERROR("Timeout on socks connect (" << proxy << " to " << remote.str() << ")");
                return std::nullopt;
            }

            if (stop_signal)
                return std::nullopt;
        }

        try
        {
            auto result = socks_result.get();
            if (!result.first)
                return {std::move(result.second)};

            LOG_ERROR("Failed to make socks connection to " << remote.str() << " (via " << proxy << "): " << result.first.message());
        }
        catch (std::future_error const&)
        {}

        return std::nullopt;
    }

  //-----------------------------------------------------------------------------------
  bool append_net_address(
      std::vector<epee::net_utils::network_address> & seed_nodes
    , std::string const & addr
    , uint16_t default_port
    )
  {
    using namespace boost::asio;

    std::string host = addr;
    std::string port = std::to_string(default_port);
    size_t colon_pos = addr.find_last_of(':');
    size_t dot_pos = addr.find_last_of('.');
    size_t square_brace_pos = addr.find('[');

    // IPv6 will have colons regardless.  IPv6 and IPv4 address:port will have a colon but also either a . or a [
    // as IPv6 addresses specified as address:port are to be specified as "[addr:addr:...:addr]:port"
    // One may also specify an IPv6 address as simply "[addr:addr:...:addr]" without the port; in that case
    // the square braces will be stripped here.
    if ((std::string::npos != colon_pos && std::string::npos != dot_pos) || std::string::npos != square_brace_pos)
    {
      net::get_network_address_host_and_port(addr, host, port);
    }
    LOG_INFO("Resolving node address: host=" << host << ", port=" << port);

    io_service io_srv;
    ip::tcp::resolver resolver(io_srv);
    ip::tcp::resolver::query query(host, port, boost::asio::ip::tcp::resolver::query::canonical_name);
    boost::system::error_code ec;
    ip::tcp::resolver::iterator i = resolver.resolve(query, ec);
    LOG_ERROR_AND_RETURN_UNLESS(!ec, false, "Failed to resolve host name '" << host << "': " << ec.message() << ':' << ec.value());

    ip::tcp::resolver::iterator iend;
    for (; i != iend; ++i)
    {
      ip::tcp::endpoint endpoint = *i;
      if (endpoint.address().is_v4())
      {
        epee::net_utils::network_address na{epee::net_utils::ipv4_network_address{boost::asio::detail::socket_ops::host_to_network_long(endpoint.address().to_v4().to_ulong()), endpoint.port()}};
        seed_nodes.push_back(na);
        LOG_INFO("Added node: " << na.str());
      }
      else
      {
        epee::net_utils::network_address na{epee::net_utils::ipv6_network_address{endpoint.address().to_v6(), endpoint.port()}};
        seed_nodes.push_back(na);
        LOG_INFO("Added node: " << na.str());
      }
    }
    return true;
  }
}
