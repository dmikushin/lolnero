// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once


#include "network/type/i2p_address.h" // needed for serialization
#include "network/type/tor_address.h" // needed for serialization

#include "tools/serialization/serialization.h"

#include "tools/epee/include/misc_language.h"
#include "tools/epee/include/net/net_utils_base.h"
#include "tools/epee/include/string_tools.h"
#include "tools/epee/include/time_helper.h"

namespace nodetool
{
  typedef boost::uuids::uuid uuid;
  typedef uint64_t peerid_type;

  std::string peerid_to_string(peerid_type peer_id);

  template<typename AddressType>
  struct peerlist_entry_base
  {
    AddressType adr;
    peerid_type id;
    int64_t last_seen;

    BEGIN_KV_SERIALIZE_MAP()
      KV_SERIALIZE(adr)
      KV_SERIALIZE(id)
      KV_SERIALIZE_OPT(last_seen, (int64_t)0)
    END_KV_SERIALIZE_MAP()

    BEGIN_SERIALIZE()
      FIELD(adr)
      FIELD(id)
      VARINT_FIELD(last_seen)
    END_SERIALIZE()
  };
  typedef peerlist_entry_base<epee::net_utils::network_address> peerlist_entry;

  template<typename AddressType>
  struct anchor_peerlist_entry_base
  {
    AddressType adr;
    peerid_type id;
    int64_t first_seen;

    BEGIN_KV_SERIALIZE_MAP()
      KV_SERIALIZE(adr)
      KV_SERIALIZE(id)
      KV_SERIALIZE(first_seen)
    END_KV_SERIALIZE_MAP()

    BEGIN_SERIALIZE()
      FIELD(adr)
      FIELD(id)
      VARINT_FIELD(first_seen)
    END_SERIALIZE()
  };
  typedef anchor_peerlist_entry_base<epee::net_utils::network_address> anchor_peerlist_entry;

  template<typename AddressType>
  struct connection_entry_base
  {
    AddressType adr;
    peerid_type id;
    bool is_income;

    BEGIN_KV_SERIALIZE_MAP()
      KV_SERIALIZE(adr)
      KV_SERIALIZE(id)
      KV_SERIALIZE(is_income)
    END_KV_SERIALIZE_MAP()

    BEGIN_SERIALIZE()
      FIELD(adr)
      FIELD(id)
      FIELD(is_income)
    END_SERIALIZE()
  };
  typedef connection_entry_base<epee::net_utils::network_address> connection_entry;

  std::string print_peerlist_to_string(const std::vector<peerlist_entry>& pl);

  struct basic_node_data
  {
    uuid network_id;
    uint32_t my_port;
    peerid_type peer_id;

    BEGIN_KV_SERIALIZE_MAP()
      KV_SERIALIZE_VAL_POD_AS_BLOB(network_id)
      KV_SERIALIZE(peer_id)
      KV_SERIALIZE(my_port)
    END_KV_SERIALIZE_MAP()
  };


  constexpr static int P2P_COMMANDS_POOL_BASE = 1000;

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  template<class t_playload_type>
	struct COMMAND_HANDSHAKE_T
	{
		const static int ID = P2P_COMMANDS_POOL_BASE + 1;

    struct request_t
    {
      basic_node_data node_data;
      t_playload_type payload_data;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(node_data)
        KV_SERIALIZE(payload_data)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<request_t> request;

    struct response_t
    {
      basic_node_data node_data;
      t_playload_type payload_data;
      std::vector<peerlist_entry> local_peerlist_new;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(node_data)
        KV_SERIALIZE(payload_data)
        KV_SERIALIZE(local_peerlist_new)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<response_t> response;
  };


  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  template<class t_playload_type>
  struct COMMAND_TIMED_SYNC_T
  {
    const static int ID = P2P_COMMANDS_POOL_BASE + 2;

    struct request_t
    {
      t_playload_type payload_data;
      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(payload_data)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<request_t> request;

    struct response_t
    {
      t_playload_type payload_data;
      std::vector<peerlist_entry> local_peerlist_new;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(payload_data)
        KV_SERIALIZE(local_peerlist_new)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<response_t> response;
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/

  struct COMMAND_PING
  {
    /*
      Used to make "callback" connection, to be sure that opponent node
      have accessible connection point. Only other nodes can add peer to peerlist,
      and ONLY in case when peer has accepted connection and answered to ping.
    */
    const static int ID = P2P_COMMANDS_POOL_BASE + 3;

#define PING_OK_RESPONSE_STATUS_TEXT "OK"

    struct request_t
    {
      /*actually we don't need to send any real data*/

      BEGIN_KV_SERIALIZE_MAP()
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<request_t> request;

    struct response_t
    {
      std::string status;
      peerid_type peer_id;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(status)
        KV_SERIALIZE(peer_id)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<response_t> response;
  };


  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct COMMAND_SUPPORT_FLAGS
  {
    const static int ID = P2P_COMMANDS_POOL_BASE + 7;

    struct request_t
    {
      BEGIN_KV_SERIALIZE_MAP()
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<request_t> request;

    struct response_t
    {
      uint32_t support_flags;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(support_flags)
      END_KV_SERIALIZE_MAP()
    };
    typedef epee::misc_utils::struct_init<response_t> response;
  };
}
