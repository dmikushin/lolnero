// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include <inttypes.h>

constexpr int64_t CORE_RPC_ERROR_CODE_WRONG_PARAM           = -1 ;
constexpr int64_t CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT        = -2 ;
constexpr int64_t CORE_RPC_ERROR_CODE_TOO_BIG_RESERVE_SIZE  = -3 ;
constexpr int64_t CORE_RPC_ERROR_CODE_WRONG_WALLET_ADDRESS  = -4 ;
constexpr int64_t CORE_RPC_ERROR_CODE_INTERNAL_ERROR        = -5 ;
constexpr int64_t CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB       = -6 ;
constexpr int64_t CORE_RPC_ERROR_CODE_BLOCK_NOT_ACCEPTED    = -7 ;
constexpr int64_t CORE_RPC_ERROR_CODE_CORE_BUSY             = -9 ;
constexpr int64_t CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB_SIZE  = -10;
constexpr int64_t CORE_RPC_ERROR_CODE_UNSUPPORTED_RPC       = -11;
constexpr int64_t CORE_RPC_ERROR_CODE_MINING_TO_SUBADDRESS  = -12;
constexpr int64_t CORE_RPC_ERROR_CODE_REGTEST_REQUIRED      = -13;
constexpr int64_t CORE_RPC_ERROR_CODE_PAYMENT_REQUIRED      = -14;
constexpr int64_t CORE_RPC_ERROR_CODE_INVALID_CLIENT        = -15;
constexpr int64_t CORE_RPC_ERROR_CODE_PAYMENT_TOO_LOW       = -16;
constexpr int64_t CORE_RPC_ERROR_CODE_DUPLICATE_PAYMENT     = -17;
constexpr int64_t CORE_RPC_ERROR_CODE_STALE_PAYMENT         = -18;
constexpr int64_t CORE_RPC_ERROR_CODE_RESTRICTED            = -19;

const char *get_rpc_server_error_message(int64_t code);
