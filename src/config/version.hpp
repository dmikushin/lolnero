#pragma once

#include <string_view>

extern const std::string_view LOLNERO_VERSION;
extern const std::string_view LOLNERO_RELEASE_NAME;
const std::string_view LOLNERO_VERSION_FULL = LOLNERO_VERSION;

